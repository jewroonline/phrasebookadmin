﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PhraseBookServices.DataAccess.EntityLayer;
using PhraseBookServices.DataAccess.Service;
using PhraseBookServices.Models;

namespace PhraseBookApi.Controllers
{
    public class TopicsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        PhrasesService phrasesService = new PhrasesService();

        // GET: api/Topics
        public IHttpActionResult GetTopics()
        {
            List<Topic> topicList = phrasesService.GetTopicList();
            List<Topic> TopicApiList = new List<Topic>();
            TopicApiList = (from x in topicList
                            select new Topic
                            {
                                AddedOn = x.AddedOn,
                                Id = x.Id,
                                IsActive = x.IsActive,
                                Name = x.Name,
                                Priority = x.Priority,
                                UpdatedOn = x.UpdatedOn,
                                Description = x.Description

                            }).ToList();

            return Ok(TopicApiList);
        }

       
        public IHttpActionResult GetphrasesByTopicId(string topicId)
        {
            Topic topic = phrasesService.TopicGetById(topicId);

            List<PhraseViewModel> PhraseApiList = new List<PhraseViewModel>();
            PhraseApiList = (from o in topic.Phrases
                             select new PhraseViewModel
                             {
                                 Id = o.Id.ToString(),
                                 PhareSubText = o.Subtext,
                                 PhareText = o.Text,
                                 TranslationEnglish = phrasesService.FindtranslationBy(2, o.Id.ToString()).TranslationText,
                                 TranslationRussian = phrasesService.FindtranslationBy(1, o.Id.ToString()).TranslationText,
                                 TranslationHebrew = phrasesService.FindtranslationBy(3, o.Id.ToString()).TranslationText,
                                 TransliterationRussian = phrasesService.FindTransliterationBy(1, o.Id.ToString()).TransliterationText,
                                 TransliterationEnglish = phrasesService.FindTransliterationBy(2, o.Id.ToString()).TransliterationText,
                                 TransliterationHebrew = phrasesService.FindTransliterationBy(3, o.Id.ToString()).TransliterationText

                             }).ToList();


            return Ok(PhraseApiList);
        }
        // GET: api/Topics/5
        [ResponseType(typeof(Topic))]
        public IHttpActionResult GetTopic(Guid id)
        {
            Topic topic = phrasesService.TopicGetById(id.ToString());
            if (topic == null)
            {
                return NotFound();
            }

            Topic topicApiClass = new Topic
            {
                AddedOn = topic.AddedOn,
                Description = topic.Description,
                Id = topic.Id,
                IsActive = topic.IsActive,
                Name = topic.Name,
                Priority = topic.Priority,
                UpdatedOn = topic.UpdatedOn
            };

            return Ok(topicApiClass);
        }

        [AcceptVerbs("GET")]
        // PUT: api/Topics/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTopic(Guid id, Topic topicApiClass, string userEmail)
        {
            Topic topic = new Topic
            {
                AddedOn = topicApiClass.AddedOn,
                Description = topicApiClass.Description,
                Id = topicApiClass.Id,
                IsActive = topicApiClass.IsActive,
                Name = topicApiClass.Name,
                Priority = topicApiClass.Priority,
                UpdatedOn = topicApiClass.UpdatedOn,
                ApplicationUser = db.Users.FirstOrDefault(x => x.Email == userEmail)

            };

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != topic.Id)
            {
                return BadRequest();
            }

            //  db.Entry(topic).State = EntityState.Modified;

            try
            {
                phrasesService.CreateTopic(topic);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TopicExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        [AcceptVerbs("GET")]
        // POST: api/Topics
        [ResponseType(typeof(Topic))]
        public IHttpActionResult PostTopic(Topic topicApiClass, string userEmail)
        {
            Topic topic = new Topic
            {
                AddedOn = topicApiClass.AddedOn,
                Description = topicApiClass.Description,
                Id = topicApiClass.Id,
                IsActive = topicApiClass.IsActive,
                Name = topicApiClass.Name,
                Priority = topicApiClass.Priority,
                UpdatedOn = topicApiClass.UpdatedOn,
                ApplicationUser = db.Users.FirstOrDefault(x => x.Email == userEmail)

            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            try
            {
                phrasesService.CreateTopic(topic);
            }
            catch (DbUpdateException)
            {
                if (TopicExists(topic.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = topic.Id }, topicApiClass);
        }

        [AcceptVerbs("GET")]
        // DELETE: api/Topics/5
        [ResponseType(typeof(Topic))]
        public IHttpActionResult DeleteTopic(Guid id)
        {
            Topic topic = phrasesService.TopicGetById(id.ToString());
            if (topic == null)
            {
                return NotFound();
            }
            else {
                phrasesService.DeleteTopic(topic.Id.ToString());
                Topic topicApiClass = new Topic
                {
                    AddedOn = topic.AddedOn,
                    Description = topic.Description,
                    Id = topic.Id,
                    IsActive = topic.IsActive,
                    Name = topic.Name,
                    Priority = topic.Priority,
                    UpdatedOn = topic.UpdatedOn
                };

                return Ok(topicApiClass);
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TopicExists(Guid id)
        {
            return db.Topics.Count(e => e.Id == id) > 0;
        }
    }
}