﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PhraseBookServices.DataAccess.EntityLayer;

namespace PhraseBookApi.Controllers
{
    public class PhrasesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Phrases
        public IQueryable<Phrase> GetPhrases()
        {
            return db.Phrases;
        }

        // GET: api/Phrases/5
        [ResponseType(typeof(Phrase))]
        public IHttpActionResult GetPhrase(Guid id)
        {
            Phrase phrase = db.Phrases.Find(id);
            if (phrase == null)
            {
                return NotFound();
            }

            return Ok(phrase);
        }

        // PUT: api/Phrases/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPhrase(Guid id, Phrase phrase)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != phrase.Id)
            {
                return BadRequest();
            }

            db.Entry(phrase).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PhraseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Phrases
        [ResponseType(typeof(Phrase))]
        public IHttpActionResult PostPhrase(Phrase phrase)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Phrases.Add(phrase);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (PhraseExists(phrase.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = phrase.Id }, phrase);
        }

        // DELETE: api/Phrases/5
        [ResponseType(typeof(Phrase))]
        public IHttpActionResult DeletePhrase(Guid id)
        {
            Phrase phrase = db.Phrases.Find(id);
            if (phrase == null)
            {
                return NotFound();
            }

            db.Phrases.Remove(phrase);
            db.SaveChanges();

            return Ok(phrase);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PhraseExists(Guid id)
        {
            return db.Phrases.Count(e => e.Id == id) > 0;
        }
    }
}