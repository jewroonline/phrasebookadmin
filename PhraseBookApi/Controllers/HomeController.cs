﻿using PhraseBookServices.DataAccess.EntityLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PhraseBookApi.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            //var db = new ApplicationDbContext();
            //db.Phrases.Add(new Phrase
            //{
            //    Subtext = "sub",
            //    Text = "text"
            //});
            //db.SaveChanges();
            return View();
        }
    }
}
