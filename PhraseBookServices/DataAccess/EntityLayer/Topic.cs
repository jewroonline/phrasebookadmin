﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace PhraseBookServices.DataAccess.EntityLayer
{
    public class Topic : BaseEntity<Guid>
    {
        [Required]
        public virtual string Name { get; set; }
        public string Description { get; set; }
        public virtual int Priority { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
        public virtual IList<Phrase> Phrases { get; set; }

    }

  
    
  
   
   

   
}
