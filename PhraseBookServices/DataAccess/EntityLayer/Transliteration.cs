﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace PhraseBookServices.DataAccess.EntityLayer
{
    public class Transliteration
    {
        public virtual Guid Id { get; set; }
        [Required]
        public virtual Phrase Phrase { get; set; }
        public virtual Language Language { get; set; }
        public virtual Gender Gender { get; set; }

       // [Required]
        public virtual string TransliterationText { get; set; }
    }
}
