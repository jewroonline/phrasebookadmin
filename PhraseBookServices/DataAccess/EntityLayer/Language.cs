﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace PhraseBookServices.DataAccess.EntityLayer
{
    public class Language
    {
        public virtual int Id { get; set; }
        [Required]
        public virtual string LanguageName { get; set; }
    }
}
