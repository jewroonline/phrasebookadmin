﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using PhraseBookServices.DataAccess.EntityLayer;

namespace PhraseBookServices.DataAccess.EntityLayer
{
    public class Phrase
    {
        public virtual Guid Id { get; set; }
        [Required]
        public virtual string Text { get; set; }
        public virtual string Subtext { get; set; }
        public virtual IList<Topic> Topics {get ;set;}        
    
    }
}
        