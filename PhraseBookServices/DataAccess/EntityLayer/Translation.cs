﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace PhraseBookServices.DataAccess.EntityLayer
{
    public class Translation
    {
        public virtual Guid Id { get; set; }

        public virtual string TranslationText { get; set; }
        [Required]
        public virtual Phrase Phrase { get; set; }
        //[Required]
        //public virtual Topic Topic { get; set; }
        [Required]
        public virtual Language Language { get; set; }
        public virtual Gender Gender { get; set; }
    }
}
