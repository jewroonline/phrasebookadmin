﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhraseBookServices.DataAccess.EntityLayer
{
    public class BaseEntity<T>
    {
        public T Id { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual DateTime AddedOn { get; set; }
        public virtual DateTime UpdatedOn { get; set; }

        public BaseEntity()
        {
            this.AddedOn = System.DateTime.Now;
            this.UpdatedOn = DateTime.Now;
        }
    }
}