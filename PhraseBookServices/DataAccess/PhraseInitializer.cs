﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhraseBookServices.Models;
using PhraseBookServices.DataAccess.EntityLayer;


namespace PhraseBookServices.DataAccess
{
    public class PhraseInitializer : DropCreateDatabaseIfModelChanges<ApplicationDbContext>
    {
        protected override async void Seed(ApplicationDbContext context)
        {
            var Genders = new List<Gender>
            {
                new Gender { Name = "Female"  },
                new Gender { Name = "Male" }
                
            };

            var Languages = new List<Language>
            {
                new Language { LanguageName = "Russian"  },
                new Language { LanguageName = "English" },
                new Language { LanguageName = "Hebrew" },
                new Language { LanguageName = "Juhuri" }
            };
            foreach (var lang in Languages)
            {
                context.Languages.Add(lang);
            }
            foreach (var gender in Genders)
            {
                context.Genders.Add(gender);
            }

            context.SaveChanges();
        }
    }
}
