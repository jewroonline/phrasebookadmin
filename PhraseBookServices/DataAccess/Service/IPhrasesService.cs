﻿
using PhraseBookServices.DataAccess.EntityLayer;
using PhraseBookServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhraseBookServices.DataAccess.Service
{
   public interface IPhrasesService
    {
        //Topic Functionality declaration
        int CreateTopic(Topic topic);
        int DeleteTopic(string Id);
        Topic TopicGetById(string Id);
        List<Topic> GetTopicList();

        // Phrase Functionality declaration

        int CreatePhrase(PhraseViewModel phrase);
        int DeletePhrase(string Id);
        Phrase PhraseGetById(string Id);
        List<Phrase> GetPhraseList();

        // Translation Functionality declaration

        int CreateTranslation(Translation phrase);
        int DeleteTranslation(string Id);
        Translation TranslationGetById(string Id);
        List<Translation> GetTranslationList();

        // Translation Functionality declaration

        int CreateTransliteration(Transliteration phrase);
        int DeleteTransliteration(string Id);
        Transliteration TransliterationGetById(string Id);
        List<Transliteration> GetTransliterationList();
    }
}
