﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PhraseBookServices.DataAccess.EntityLayer;
using PhraseBookServices.Models;
using System.Data.Entity.Validation;

namespace PhraseBookServices.DataAccess.Service
{
    public class PhrasesService : IPhrasesService
    {
        ApplicationDbContext context = new ApplicationDbContext();
        public int CreatePhrase(PhraseViewModel model)
        {
            if (model.Id == null)
            {
                #region Add New Value
                Phrase phrases = new Phrase
                {
                    Id = Guid.NewGuid(),
                    Text = model.PhareText,
                    Subtext=model.PhareSubText,
                    Topics = new List<Topic>()

                };
                context.Phrases.Add(phrases);
                context.SaveChanges();

                var gender = context.Genders.FirstOrDefault(x => x.Id == 1);

                var Russian = context.Languages.FirstOrDefault(x => x.Id == 1);
                var English = context.Languages.FirstOrDefault(x => x.Id == 2);
                var Hebrew = context.Languages.FirstOrDefault(x => x.Id == 3);
                var Juhuri = context.Languages.FirstOrDefault(x => x.Id == 4);

                List<Translation> Translation = new List<Translation> {
                new Translation {
                TranslationText = model.TranslationRussian,
                Gender = gender,
                Language = Russian,
                Phrase = phrases
                },
                new Translation {
                TranslationText = model.TranslationEnglish,
                Gender = gender,
                Language = English,
                Phrase = phrases
                },
                 new Translation {
                TranslationText = model.TranslationHebrew,
                Gender = gender,
                Language = Hebrew,
                Phrase = phrases
                }
            };

                foreach (var item in Translation)
                {
                    item.Id = Guid.NewGuid();
                    context.Translations.Add(item);
                    context.SaveChanges();
                }

                List<Transliteration> Transliteration = new List<Transliteration> {
                new Transliteration {
                TransliterationText = model.TranslationRussian,
                Gender = gender,
                Language = Russian,
                Phrase = phrases
                },
                new Transliteration {
                TransliterationText = model.TranslationEnglish,
                Gender = gender,
                Language = English,
                Phrase = phrases
                },
                 new Transliteration {
                TransliterationText = model.TranslationHebrew,
                Gender = gender,
                Language = Hebrew,
                Phrase = phrases
                }
            };

                foreach (var item in Transliteration)
                {
                    item.Id = Guid.NewGuid();
                    context.Transliterations.Add(item);
                    context.SaveChanges();
                }


                foreach (var item in model.selectedTopics)
                {
                    var topic = context.Topics.FirstOrDefault(x => x.Id == new Guid(item));
                    context.Topics.Attach(topic);
                    phrases.Topics.Add(topic);
                    context.SaveChanges();
                }
                return 1;
                #endregion
            }
            else
            {
                #region Edit Data
                var phrase = context.Phrases.FirstOrDefault(x=>x.Id==new Guid(model.Id));
                List< Topic > Topics = new List<Topic>();
                    if(phrase.Topics.Count()>0)
                { 
                    Topics =phrase.Topics.ToList();
                }
                phrase.Text = model.PhareText;
                    phrase.Subtext = model.PhareSubText; 
                 
              
                context.SaveChanges();

                var gender = context.Genders.FirstOrDefault(x => x.Id == 1);

                var Russian = context.Languages.FirstOrDefault(x => x.Id == 1);
                var English = context.Languages.FirstOrDefault(x => x.Id == 2);
                var Hebrew = context.Languages.FirstOrDefault(x => x.Id == 3);
                var Juhuri = context.Languages.FirstOrDefault(x => x.Id == 4);
                try
                {
                    var TranslationRussian = context.Translations.FirstOrDefault(x => x.Phrase.Id == new Guid(model.Id) && x.Language.Id == Russian.Id);
                    TranslationRussian.TranslationText = model.TranslationRussian;
                    context.Entry(TranslationRussian.Phrase).State = System.Data.Entity.EntityState.Unchanged;
                    context.SaveChanges();
                  
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                }
                

                var TranslationEnglish = context.Translations.FirstOrDefault(x => x.Phrase.Id == new Guid(model.Id) && x.Language.Id == English.Id);
                TranslationEnglish.TranslationText = model.TransliterationEnglish;
                context.SaveChanges();

                var TranslationHebrew = context.Translations.FirstOrDefault(x => x.Phrase.Id == new Guid(model.Id) && x.Language.Id == Hebrew.Id);
                TranslationHebrew.TranslationText = model.TransliterationHebrew;
                context.SaveChanges();



                var TransliterationRussian = context.Transliterations.FirstOrDefault(x => x.Phrase.Id == new Guid(model.Id) && x.Language.Id == Russian.Id);
                var TransliterationEnglish = context.Transliterations.FirstOrDefault(x => x.Phrase.Id == new Guid(model.Id) && x.Language.Id == English.Id);
                var TransliterationHebrew = context.Transliterations.FirstOrDefault(x => x.Phrase.Id == new Guid(model.Id) && x.Language.Id == Hebrew.Id);

                if (TransliterationRussian!=null)
                {
                    TransliterationRussian.TransliterationText = model.TranslationRussian;
                    context.SaveChanges();
                }
                if (TransliterationEnglish != null)
                {
                    TransliterationEnglish.TransliterationText = model.TransliterationEnglish;
                    context.SaveChanges();
                }
                if (TransliterationHebrew != null)
                {
                    TransliterationHebrew.TransliterationText = model.TransliterationHebrew;
                    context.SaveChanges();
                }
            



            foreach (var item in Topics)
                {
                    phrase.Topics.Remove(item);
                    context.SaveChanges();
                }
                
                foreach (var item in model.selectedTopics)
                {
                    var topic = context.Topics.FirstOrDefault(x=>x.Id==new Guid(item));
                     context.Topics.Attach(topic);
                    phrase.Topics.Add(topic);
                    context.SaveChanges();
                }
                return 2;
                #endregion

            }
            
           
        }

     
        public int CreateTopic(Topic topic)
        {
            Topic topics = context.Topics.FirstOrDefault(x => x.Id == topic.Id);
            if (topics != null)
            {
                topics.Name = topic.Name;
                topics.Description = topic.Description;
                topics.UpdatedOn = System.DateTime.Now;
                context.SaveChanges();
                return 2;
            }
            else
            {
                topic.Id = Guid.NewGuid();
                topic.ApplicationUser = context.Users.FirstOrDefault(x => x.UserName == topic.ApplicationUser.UserName);
                context.Topics.Add(topic);
                context.SaveChanges();
                return 1;
            }

           
        }

        public int CreateTranslation(Translation phrase)
        {
            throw new NotImplementedException();
        }

        public int CreateTransliteration(Transliteration phrase)
        {
            throw new NotImplementedException();
        }

        public int DeletePhrase(string Id)
        {
            Phrase phrase =context.Phrases.FirstOrDefault(x=>x.Id==new Guid(Id));
            if (phrase != null)
            {
                context.Phrases.Remove(phrase);
                context.SaveChanges();
                return 0;
            }
            else {
                return 4;
            }
        }

        public int DeleteTopic(string Id)
        {
            Topic topic = context.Topics.FirstOrDefault(x => x.Id ==new Guid (Id));
            if (topic != null)
            {
                context.Topics.Remove(topic);
                context.SaveChanges();
                return 0;
            }
            else {
                return 4;
            }
        }

        public int DeleteTranslation(string Id)
        {
            Topic topic = context.Topics.FirstOrDefault(x => x.Id == new Guid(Id));

            if (topic != null)
            {
                context.Topics.Remove(topic);
                context.SaveChanges();
                return 0;
            }
            else {
                return 1;
            }
        }

        public int DeleteTransliteration(string Id)
        {
            throw new NotImplementedException();
        }

        public List<Phrase> GetPhraseList()
        {
            return context.Phrases.OrderByDescending(s => s.Text).ToList();
        }

        public List<Topic> GetTopicList()
        {
            return context.Topics.OrderBy(s=>s.Name).ToList();
        }

        public List<Translation> GetTranslationList()
        {
            throw new NotImplementedException();
        }

        public List<Transliteration> GetTransliterationList()
        {
            throw new NotImplementedException();
        }

        public Phrase PhraseGetById(string Id)
        {
            
                return context.Phrases.FirstOrDefault(a => a.Id == new Guid(Id));
          
        }

        public Topic TopicGetById(string Id)
        {
                return context.Topics.FirstOrDefault(a => a.Id == new Guid(Id));
          
        }

        public Translation TranslationGetById(string Id)
        {
            throw new NotImplementedException();
        }

        public Transliteration TransliterationGetById(string Id)
        {
            throw new NotImplementedException();
        }

        public Translation FindtranslationBy(int lagid, string Phraseid)
        {
            return context.Translations.FirstOrDefault(x => x.Language.Id == lagid && x.Phrase.Id == new Guid(Phraseid));
        }

        public Transliteration FindTransliterationBy(int lagid, string Phraseid)
        {
            return context.Transliterations.FirstOrDefault(x => x.Language.Id == lagid && x.Phrase.Id == new Guid(Phraseid));
        }
    }
}

