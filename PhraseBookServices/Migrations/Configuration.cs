namespace PhraseBookServices.Migrations
{
    using DataAccess.EntityLayer;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<PhraseBookServices.DataAccess.EntityLayer.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(PhraseBookServices.DataAccess.EntityLayer.ApplicationDbContext context)
        {
            var Genders = new List<Gender>
            {
                new Gender { Name = "Female"  },
                new Gender { Name = "Male" }

            };

            var Languages = new List<Language>
            {
                new Language { LanguageName = "Russian"  },
                new Language { LanguageName = "English" },
                new Language { LanguageName = "Hebrew" },
                new Language { LanguageName = "Juhuri" }
            };
            foreach (var lang in Languages)
            {
                context.Languages.Add(lang);
            }
            foreach (var gender in Genders)
            {
                context.Genders.Add(gender);
            }

            context.SaveChanges();
        }
    }
}
