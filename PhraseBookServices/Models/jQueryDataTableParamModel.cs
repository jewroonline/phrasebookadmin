﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhraseBookServices.Models
{
    public class jQueryDataTableParamModel
    {
        /// <summary>
        /// Запрос порядковый номер послан DataTable, то же самое значение должно быть возвращено в ответ
        /// </summary>       
        public string sEcho { get; set; }

        /// <summary>
        /// Текст, используемый для фильтрации
        /// </summary>
        public string sSearch { get; set; }

        /// <summary>
        /// Число записей, которые должны быть показаны в таблице
        /// </summary>
        public int iDisplayLength { get; set; }

        /// <summary>
        /// Первая запись, что должно быть показано (используется для просмотра)
        /// </summary>
        public int iDisplayStart { get; set; }

        /// <summary>
        /// Количество столбцов в таблице
        /// </summary>
        public int iColumns { get; set; }

        /// <summary>
        /// Количество столбцов, которые используются при сортировке
        /// </summary>
        public int iSortingCols { get; set; }

        /// <summary>
        /// Разделенный запятыми список имен столбцов
        /// </summary>
        public string sColumns { get; set; }

        public int iSortCol_0 { get; set; } // the first (and usually only) column to be sorted by

        public string sSortDir_0 { get; set; }

    }
}