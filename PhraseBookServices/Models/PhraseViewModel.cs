﻿using PhraseBookServices.DataAccess.EntityLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhraseBookServices.Models
{
    public class PhraseViewModel
    {    
        public string Id { get; set; }
        public string PhareText { get; set; }
        public string PhareSubText { get; set; }
        public string TranslationEnglish { get; set; }
        public string TransliterationEnglish { get; set; }
        public string TranslationRussian { get; set; }
        public string TransliterationRussian { get; set; }
        public string TranslationHebrew { get; set; }
        public string TransliterationHebrew { get; set; }
      

        //public virtual Phrase Phrase { get; set; }        
        //public virtual Translation translation { get; set; }
        //public virtual Transliteration transliteration { get; set; }

        public virtual string[] selectedTopics { get; set; }

        public virtual List<Topic> Topics { get; set; }
        
    }
}
