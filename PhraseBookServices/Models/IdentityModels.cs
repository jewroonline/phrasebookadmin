﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using PhraseBookServices.DataAccess.EntityLayer;
using PhraseBookServices.DataAccess;


namespace PhraseBookServices.DataAccess.EntityLayer
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
       
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Phrase> Phrases { get; set; }
        public DbSet<Topic> Topics { get; set; }
        public DbSet<Translation> Translations { get; set; }

        public DbSet<Transliteration> Transliterations { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<Gender> Genders { get; set; }
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            // connetionString = "Data Source=ServerName;Initial Catalog=DatabaseName;User ID=UserName;Password=Password";

           // Database.SetInitializer<ApplicationDbContext>(null);
           // Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApplicationDbContext,PhraseBookA>("PhrasesDBContext"));
        
    }
       
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

       // public System.Data.Entity.DbSet<PhraseBookAdmin.DataAccess.EntityLayer.ApplicationUser> ApplicationUsers { get; set; }
    }
}