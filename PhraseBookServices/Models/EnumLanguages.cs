﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhraseBookServices.Models
{
    public enum Languages  : int
    {

        Russian = 1,
        English = 2,
        Hebrew = 3,
        Juhuri = 4
    }
}
