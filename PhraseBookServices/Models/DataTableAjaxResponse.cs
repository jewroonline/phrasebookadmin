﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhraseBookServices.Models
{
    public class DataTableAjaxResponse
    {
        public object AaDataDict { get; set; }
        public int iTotalRecords { get; set; }
        public int iTotalDisplayRecords { get; set; }

        public List<string> yadcf_data_1 { get; set; }
    }
}