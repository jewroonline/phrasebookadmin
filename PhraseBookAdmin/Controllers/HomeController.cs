﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PhraseBookServices.DataAccess;
using PhraseBookServices.DataAccess.EntityLayer;

namespace PhraseBookAdmin.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            //var db = new ApplicationDbContext();
            //db.Phrases.Add(new Phrase
            //{
            //    Subtext = "sub",
            //    Text = "text"
            //});
            //db.SaveChanges();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        
    }
}