﻿using PhraseBookServices.DataAccess.EntityLayer;
using PhraseBookServices.DataAccess.Service;
using PhraseBookServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace PhraseBookAdmin.Controllers
{
    [Authorize]
    public class PhraseController : Controller
    {
        private ApplicationDbContext context = new ApplicationDbContext();       
        private PhrasesService phrasesService = new PhrasesService();

        // GET: Phrase
        public ActionResult Index(string id = "", string saveresult = "")
        {
            if (id != "")
            {
                var data = context.Phrases.FirstOrDefault(a => a.Id == new Guid(id));
                Translation TranslationRussian = context.Translations.FirstOrDefault(x => x.Language.Id == 1 && x.Phrase.Id == data.Id);
                Translation TranslationEnglish = context.Translations.FirstOrDefault(x => x.Language.Id == 1 && x.Phrase.Id == data.Id);
                Translation TranslationHebrew = context.Translations.FirstOrDefault(x => x.Language.Id == 1 && x.Phrase.Id == data.Id);
                Transliteration TransliterationEnglish = context.Transliterations.FirstOrDefault(x => x.Language.Id == 1 && x.Phrase.Id == data.Id);
                Transliteration TransliterationHebrew = context.Transliterations.FirstOrDefault(x => x.Language.Id == 1 && x.Phrase.Id == data.Id);
                Transliteration TransliterationRussian = context.Transliterations.FirstOrDefault(x => x.Language.Id == 1 && x.Phrase.Id == data.Id);

                TempData["Id"] = data.Id.ToString();
                TempData["PhareSubText"] = data.Subtext;
                TempData["PhareText"] = data.Text;
                TempData["TranslationRussian"] = TranslationRussian.TranslationText;
                TempData["TranslationEnglish"] = TranslationEnglish.TranslationText;
                TempData["TranslationHebrew"] = TranslationHebrew.TranslationText;
                TempData["TransliterationEnglish"] = TransliterationEnglish.TransliterationText;
                TempData["TransliterationHebrew"] = TransliterationHebrew.TransliterationText;
                TempData["TransliterationRussian"] = TransliterationRussian.TransliterationText;
                ViewBag.SelectedTopics = data.Topics;
            }
            if (saveresult != "")
            {
                TempData["save"] = saveresult;
            }
            List<Topic> topics = phrasesService.GetTopicList();
          
            ViewBag.Topics = topics;
            return View();
        }

        #region Topic Functionality
        public ActionResult CreateTopic(string id = "",string saveresult="")
        {
            if (id != "")
            {
                var data = context.Topics.FirstOrDefault(a => a.Id == new Guid(id));
                TempData["Id"] = data.Id;
                TempData["Name"] = data.Name;
                TempData["Description"] = data.Description;
            }
            if (saveresult != "")
            {
                TempData["save"] = saveresult;
            }
            return View();
        }
        public ActionResult SaveTopic(Topic model)
        {
            try
            {
                var userEmail = User.Identity.Name;

                if (!string.IsNullOrWhiteSpace(userEmail))
                {
                    model.ApplicationUser = new ApplicationUser { UserName = userEmail };

                   int result= phrasesService.CreateTopic(model);
                    if (result==1)
                    {
                        return RedirectToAction("CreateTopic", new { saveresult =result.ToString() });
                    }
                    if (result == 2)
                    {
                        return RedirectToAction("CreateTopic", new { saveresult = result.ToString() });
                    }

                }

                else
                {
                    return RedirectToAction("CreateTopic", new { saveresult ="3" });                  
                }
            }
            catch (Exception e)
            {
                return RedirectToAction("CreateTopic", new { saveresult = e.Message });                
            }
            return View();
        }

        public ActionResult DeleteTopic(Guid id)
        {
            int result = phrasesService.DeleteTopic(id.ToString());
            if (result == 0)
            {
                return RedirectToAction("TopicList", new { saveresult =result });
            }

            if (result==0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            return RedirectToAction("TopicList");


        }

        public ActionResult EditTopic(Guid id)
        {
            var topic = context.Topics.FirstOrDefault(x => x.Id == id);

            return RedirectToAction("CreateTopic");
        }
        public JsonResult GetTopics(jQueryDataTableParamModel param)
        {
            var aaDataDict = GetTopics(param.iDisplayStart, param.iDisplayLength, param.iSortCol_0, param.sSortDir_0, param.sSearch);
            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = aaDataDict.iTotalRecords,
                iTotalDisplayRecords = aaDataDict.iTotalDisplayRecords,
                aaData = aaDataDict.AaDataDict,
            },
            JsonRequestBehavior.AllowGet);
        }

        private DataTableAjaxResponse GetTopics(int iDisplayStart, int iDisplayLength, int iSortCol_0, string sSortDir_0, string sSearch)
        {
            var resultview = context.Topics.Count();
            var result = context.Topics.OrderByDescending(x => x.Name).AsQueryable();
            if (iSortCol_0 == 0 && sSortDir_0 == "asc")
            {
                result = context.Topics.OrderBy(x => x.Name).AsQueryable();
            }
            else if (iSortCol_0 == 1 && sSortDir_0 == "asc")
            {
                result = context.Topics.OrderBy(x => x.Description).AsQueryable();
            }
            else if (iSortCol_0 == 1 && sSortDir_0 == "desc")
            {
                result = context.Topics.OrderByDescending(x => x.Description).AsQueryable();
            }

            if (!String.IsNullOrEmpty(sSearch))
            {
                result = result.Where(x => x.Name.Contains(sSearch) || x.Description.Contains(sSearch));
            }

            var aaDataDict = result.Skip(iDisplayStart).Take(iDisplayLength)
                .ToList()
                .Select(x => new
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description

                }).ToList();

            return new DataTableAjaxResponse() { AaDataDict = aaDataDict, iTotalDisplayRecords = resultview, iTotalRecords = resultview };
        }

        public ActionResult TopicList( string saveresult = "")
        {           
            if (saveresult != "")
            {
                TempData["save"] = saveresult;
            }
            return View();
        }

        #endregion

        #region Phrase Functionality
        public ActionResult SavePhrase(PhraseViewModel model)
        {
            int result = phrasesService.CreatePhrase(model);
            if (result == 1)
            {
                return RedirectToAction("Index", new { saveresult = result.ToString()});
            }
            if (result == 2)
            {
                return RedirectToAction("Index", new { saveresult =result.ToString()});
            }
            return RedirectToAction("Index");
        }

        public ActionResult DeletePhrase(Guid id)
        {
            int result = phrasesService.DeletePhrase(id.ToString());
            if (result == 4)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (result == 0)
            {
                return RedirectToAction("PhraseList", new { deleteresult =result.ToString() });
            }
            return RedirectToAction("PhraseList");
        }

        public ActionResult EditPhrase(Guid id)
        {
            var phrase = phrasesService.DeletePhrase(id.ToString());

            return RedirectToAction("Index");
        }

        public JsonResult GetPhrases(jQueryDataTableParamModel param)
        {
            var aaDataDict = GetPhrases(param.iDisplayStart, param.iDisplayLength, param.iSortCol_0, param.sSortDir_0, param.sSearch);
            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = aaDataDict.iTotalRecords,
                iTotalDisplayRecords = aaDataDict.iTotalDisplayRecords,
                aaData = aaDataDict.AaDataDict,
            },
            JsonRequestBehavior.AllowGet);
        }

        private DataTableAjaxResponse GetPhrases(int iDisplayStart, int iDisplayLength, int iSortCol_0, string sSortDir_0, string sSearch)
        {
            var resultview = context.Phrases.Count();
            var result = context.Phrases.OrderByDescending(x => x.Text).AsQueryable();
            if (iSortCol_0==0 && sSortDir_0=="asc")
            {
                result = context.Phrases.OrderBy(x => x.Text).AsQueryable();
            }           

            if (!String.IsNullOrEmpty(sSearch))
            {
                result = result.Where(x => x.Text.Contains(sSearch));
            }

           
            var aaDataDict = result.Skip(iDisplayStart).Take(iDisplayLength)
                .ToList()
                .Select(x => new
                {
                    Id = x.Id,
                    Name = x.Text,
                    // PhareSubText=x.Subtext,
                    Categories = x.Topics.Select(y=>y.Name),
                    //TranslationRussian=context.Translations.Where(s=>s.Phrase.Id==x.Id && s.Language.Id==1).Select(y=>y.TranslationText),
                    //TranslationEnglish = context.Translations.Where(s => s.Phrase.Id == x.Id && s.Language.Id == 2).Select(y => y.TranslationText),
                    //TranslationHebrew = context.Translations.Where(s => s.Phrase.Id == x.Id && s.Language.Id == 3).Select(y => y.TranslationText),

                    //TransliterationRussian = context.Transliterations.Where(s => s.Phrase.Id == x.Id && s.Language.Id == 1).Select(y=>y.TransliterationText),
                    //TransliterationEnglish = context.Transliterations.Where(s => s.Phrase.Id == x.Id && s.Language.Id == 2).Select(y => y.TransliterationText),
                    //TransliterationHebrew = context.Transliterations.Where(s => s.Phrase.Id == x.Id && s.Language.Id == 3).Select(y => y.TransliterationText),

                }).ToList();
           
            return new DataTableAjaxResponse() { AaDataDict = aaDataDict, iTotalDisplayRecords = resultview, iTotalRecords = resultview };
        }

        public ActionResult PhraseList(string deleteresult = "")
        {
            if (deleteresult != "")
            {
                TempData["save"] = deleteresult;
            }
            return View();
        }

        #endregion

    }
}