﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PhraseBookAdmin.Startup))]
namespace PhraseBookAdmin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
