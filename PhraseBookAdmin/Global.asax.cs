﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using PhraseBookServices.DataAccess;
using PhraseBookServices.Models;
using PhraseBookServices.DataAccess.EntityLayer;
using System.Web.Http;

namespace PhraseBookAdmin
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            //  Database.SetInitializer(new DropCreateDatabaseIfModelChanges<ApplicationDbContext>());
            // Database.SetInitializer<ApplicationDbContext>(new PhraseInitializer());
          //  Database.SetInitializer<ApplicationDbContext>(null);
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
           // WebApiConfig.Register(GlobalConfiguration.Configuration);
           

        }
    }
}
